FROM registry.gitlab.com/islandoftex/images/texlive:latest

ARG SSH=openssh-client
ARG PIP=python3-pip
ARG SSHPASS=sshpass

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
  "$PIP$" \
  "$SSH$" \
  "$SSHPASS"
RUN pip3 install GitPython PyYAML
