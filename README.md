# TeX Live docker image

This is a quick docker image used in my CI builds based on the one from
[Island of TeX][iot].

I build this to include some python packages as well as ssh support into my image.
For details on the image, refer to their repository.

[iot]: https://gitlab.com/islandoftex/images/texlive
